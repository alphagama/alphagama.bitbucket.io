/* eslint-disable global-require,new-cap */
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const path = require('path');
const PnpWebpackPlugin = require('pnp-webpack-plugin');
const safePostCssParser = require('postcss-safe-parser');
const TerserPlugin = require('terser-webpack-plugin');
const writeAssetsWebpackPlugin = require('write-assets-webpack-plugin');
const loaderUtils = require('loader-utils');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");
const { type } = require('os');

const sassRegex = /\.(css|scss|sass)$/;
const sassModuleRegex = /\.module\.(css|scss|sass)$/;
const isEnvDevelopment = process.env.NODE_ENV === 'development';
const isEnvProduction = !isEnvDevelopment;

function getLocalIdent(
  context,
  localIdentName,
  localName,
  options
) {
  const fileNameOrFolder = context.resourcePath.match(
    /index\.module\.(css|scss|sass)$/
  )
    ? '[folder]'
    : '[name]';
  const className = loaderUtils.interpolateName(
    context,
    `${fileNameOrFolder}_${localName}`,
    options
  );
  return className.replace('.module_', '_');
}

const cssModulesConfig = {
  importLoaders: 2,
  modules: true,
};

const getStyleLoaders = (cssOptions, preProcessor) => {
  const loaders = [
    {
      loader: MiniCssExtractPlugin.loader,
      options: {
        publicPath: '../'
      }
    },
    {
      loader: require.resolve('css-loader'),
      options: cssOptions
    },
    {
      loader: require.resolve('resolve-url-loader')
    },
    {
      loader: require.resolve('postcss-loader'),
      options: {
        ident: 'postcss',
        plugins: () => [
          require('postcss-flexbugs-fixes'),
          require('postcss-preset-env')({
            autoprefixer: {
              flexbox: 'no-2009'
            },
            stage: 3
          })
        ],
      }
    }
  ].filter(Boolean);
  if (preProcessor) {
    loaders.push({
      loader: require.resolve(preProcessor),
    });
  }
  return loaders;
};

const getPlugins = () => {
  const plugins = [
    new HtmlWebpackPlugin(
      {
        inject: true,
        template: './index.html',
      }
    ),
    new CaseSensitivePathsPlugin(),
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: 'css/alphagama.[contenthash].css'
    }),
    new CopyPlugin({
      patterns: [
        { from: "src/assets", to: "assets" },
        { from: "src/css", to: "css", globOptions: {
          ignore: ["**/**/alphagama.css"],
        }, },
        { from: "src/bootstrap", to: "bootstrap" }
      ],
    }),
  ];
  if (isEnvDevelopment) {
    plugins.push(new writeAssetsWebpackPlugin({
      force: true,
      extension: ['js', 'css', 'svg', 'html', 'jpeg', 'jpg', 'png']
    }));
  }
  return plugins;
};

module.exports = () => {
  return {
    target: 'web',
    mode: isEnvDevelopment ? 'development' : 'production',
    bail: isEnvProduction,
    devtool: isEnvDevelopment ? 'source-map' : 'cheap-module-source-map',
    entry: ['@babel/polyfill', './src/alphagama.js'],
    output: {
      path: path.resolve('./dist/'),
      pathinfo: isEnvDevelopment,
      filename: 'alphagama.[contenthash].js',
      publicPath: '',
      devtoolModuleFilenameTemplate: isEnvProduction
        ? (info) => path
          .relative('./src', info.absoluteResourcePath)
          .replace(/\\/g, '/')
        : isEnvDevelopment
        && ((info) => path.resolve(info.absoluteResourcePath).replace(/\\/g, '/'))
    },
    optimization: {
      minimize: isEnvProduction,
      minimizer: [
        new TerserPlugin({
          terserOptions: {
            parse: {
              ecma: 8
            },
            compress: {
              ecma: 5,
              warnings: false,
              comparisons: false,
              inline: 2
            },
            mangle: {
              safari10: true
            },
            output: {
              ecma: 5,
              comments: false,
              ascii_only: true
            }
          },
          parallel: true,
          cache: true,
          sourceMap: isEnvDevelopment
        }),
        new OptimizeCSSAssetsPlugin({
          cssProcessorOptions: {
            parser: safePostCssParser,
            map: isEnvDevelopment
              ? {
                inline: false,
                annotation: true
              }
              : false
          }
        })
      ]
    },
    resolve: {
      extensions: ['.js', '.json', '.jsx'],
      plugins: [PnpWebpackPlugin]
    },
    resolveLoader: {
      plugins: [PnpWebpackPlugin.moduleLoader(module)]
    },
    module: {
      rules: [
        {
          parser: {
            requireEnsure: false
          }
        },
        {
          oneOf: [
            {
              test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/, /\.svg$/, /\.eot$/, /\.woff2$/, /\.woff$/, /\.ttf$/],
              loader: require.resolve('url-loader'),
              options: {
                limit: 10000,
                publicPath: 'assets/',
                name: 'assets/img/[name].[ext]'
              }
            },
            {
              test: /\.(js|jsx)$/,
              loader: require.resolve('babel-loader')
            },
            {
              test: sassRegex,
              exclude: sassModuleRegex,
              use: getStyleLoaders(
                {
                  importLoaders: 2,
                  sourceMap: isEnvDevelopment
                },
                'sass-loader'
              ),
              sideEffects: true
            },
            {
              test: sassModuleRegex,
              use: getStyleLoaders(isEnvDevelopment
                ? {
                  ...cssModulesConfig,
                  modules: { getLocalIdent }
                } : {
                  ...cssModulesConfig,
                  modules: {
                    localIdentName: '[sha1:hash:hex:4]'
                  }
                },
              'sass-loader')
            },
            {
              loader: require.resolve('file-loader'),
              exclude: [/\.(js|mjs|jsx|ts|tsx)$/, /\.html$/, /\.json$/],
              options: {
                name: 'static/media/[name].[hash:8].[ext]'
              }
            }
          ]
        }
      ]
    },
    plugins: getPlugins(),
    devServer:
      {
        contentBase: path.join(__dirname, 'dist'),
        public: 'http://localhost:2021'
      }
  };
};
