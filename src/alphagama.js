import './css/alphagama.css';
import './automaticSlider.js';

$('#year').text(new Date().getFullYear());

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    document.getElementById("header-nav-bar").style.padding = "0";
  } else {
    document.getElementById("header-nav-bar").style.paddingTop = ".5rem";
    document.getElementById("header-nav-bar").style.paddingBottom = ".5rem";
  }
} 