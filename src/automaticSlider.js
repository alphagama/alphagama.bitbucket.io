var scrollMonitor = require("scrollmonitor");
const featureNavLink = '.nav-tabs > .nav-item > .nav-link';

function moveTheActiveComponent(inputs) {
    inputs.filter('.active').each((index, element) => {
        const currentElementIndex = inputs.index(element);
        const figureId = element.href.split('#')[1];
        $(element).removeClass('active');
        $('#'+figureId).removeClass('active');
        const nextIndex = (currentElementIndex + 1) % inputs.length;
        const nextElement = inputs[nextIndex];
        const nextFigureId = nextElement.href.split('#')[1];
        $(nextElement).addClass('active');
        $('#'+nextFigureId).addClass('active');
    });
}
function setIntervalTimerToActiveComponents(containerArray) {
    return setInterval(function () { moveTheActiveComponent(containerArray) }, 22000);
}

var elementWatcher1 = scrollMonitor.create( document.getElementById("features-1") );
elementWatcher1.enterViewport(function() {
    const containers = $('.first-container *> ' + featureNavLink);
    var interval = setIntervalTimerToActiveComponents(containers);
    $(containers).click(function(){
        clearInterval(interval)
        interval = setIntervalTimerToActiveComponents(containers);
    })
});

var elementWatcher2 = scrollMonitor.create( document.getElementById("features-2") );
elementWatcher2.enterViewport(function() {
    const containers = $('.second-container *> ' + featureNavLink);
    var interval = setIntervalTimerToActiveComponents(containers);
    $(containers).click(function(){
        clearInterval(interval)
        interval = setIntervalTimerToActiveComponents(containers);
    })
});